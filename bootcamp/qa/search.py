from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Date, Search
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch
from . import models


es = Elasticsearch(
    ['https://elastic:EjZJG3jfAmEa7BfT8Sodf3VN@9ca6b03b47874eabbba6615668eeb495.ap-southeast-1.aws.found.io:9243'],

)
#connections.configure(default={'hosts': ['https://elastic:EjZJG3jfAmEa7BfT8Sodf3VN@9ca6b03b47874eabbba6615668eeb495.ap-southeast-1.aws.found.io:9243']})
connections.create_connection(
    hosts=['https://elastic:EjZJG3jfAmEa7BfT8Sodf3VN@9ca6b03b47874eabbba6615668eeb495.ap-southeast-1.aws.found.io:9243'], timeout=20)


class QuestionIndex(DocType):
    user = Text()
    timestamp = Date()
    title = Text()
    content = Text()
    id = Text()

    class Index:
        name = 'question-index'


def bulk_indexing():

    for b in models.Question.objects.all():
        b.indexing()


def search(query):
    s = Search(index='question-index').query("query_string",
                                             query='*'+query+'*', fields=['title', 'content'])
    response = s.execute()
    set_of_id = set()
    for i in response:
        set_of_id.add(i['id'])
    return set_of_id
