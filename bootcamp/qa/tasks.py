from __future__ import absolute_import, unicode_literals
from celery.decorators import task
from celery import shared_task
from bootcamp.qa.models import Question

import json
import os


@shared_task
def download_questions(user_id):
    question_lists = []
    filename = 'questions_from_user_'+str(user_id)+'.json'
    filepath = os.path.join(os.environ.get('HOME'), filename)
    obj = Question.objects.filter(user=user_id)
    for question in obj:
        
        question_dict = {}
        question_dict["title"] = question.title
        question_dict["content"] = question.content
        question_lists.append(question_dict)
    print(question_lists)
    with open(filepath, "w+") as f:
        json.dump(question_lists, f)



